'use strict';

/**
 * @ngdoc overview
 * @name srbkWebApp
 * @description
 * # srbkWebApp
 *
 * Main module of the application.
 */
angular.module('srbkWebApp', [
        'ngAnimate',
        'ngCookies',
        'ngMessages',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch'
    ])
    .factory('moviesFactory', function ($resource) {
        return $resource('/srbk/rest/products');
    })
    .config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/main.html',
                controller: 'MainCtrl'
            })
            .when('/movie/:id', {
                templateUrl: 'views/movie.html',
                controller: 'movieDetailsCtrl'
            })
            .when('/about', {
                templateUrl: 'views/about.html',
                controller: 'AboutCtrl'
            })
            .when('/contact', {
                templateUrl: 'views/contact.html',
                controller: 'ContactCtrl'
            })
            .otherwise({
                redirectTo: '/'
            });
    });
