srbkWebApp.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'views/main.html',
            controller: 'MainCtrl'
        })
        .when('/movie/:id', {
            templateUrl: 'views/movie.html',
            controller: 'movieDetailsCtrl'
        })
        .when('/bookTickets/:id', {
            templateUrl: 'views/seances.html',
            controller: 'bookTicketsController'
        })
        .when('/bookTickets/:id/:sid', {
            templateUrl: 'views/seats.html',
            controller: 'seatsChooseController'
        })
        .when('/bookTickets/:id/:sid/:prid', {
            templateUrl: 'views/doreservation.html',
            controller: 'reservationController'
        })
        .when('/bookTickets/:id/:sid/:prid/confirm', {
            templateUrl: 'views/confirmreservation.html',
            controller: 'reservationController'
        })
        .when('/bookTickets/:id/:sid/:prid/done', {
            templateUrl: 'views/donereservation.html',
            controller: 'reservationController'
        })
        .when('/about', {
            templateUrl: 'views/about.html',
            controller: 'AboutCtrl'
        })
        .when('/contact', {
            templateUrl: 'views/contact.html',
            controller: 'ContactCtrl'
        })
        .otherwise({
            redirectTo: '/'
        });
});