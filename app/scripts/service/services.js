srbkWebApp = angular.module('srbkWebApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
]);


srbkWebApp.factory('moviesFactory', function ($resource) {
    return $resource('/rest/products');
});

srbkWebApp.factory('seancesFactory', function ($resource, $routeParams) {
    return {
        query: function (product_id) {
            return $resource('/rest/seances/:id', {
                id: product_id
            }).query();
        }
    }
});

srbkWebApp.factory('seatsFactory', function ($resource, $routeParams) {
    return {
        get: function () {
            return $resource('/rest/seances/onewithoccupation/:id', {
                id: $routeParams.sid
            }).get();
        }
    }
});

srbkWebApp.factory('reservationsFactory', function ($resource, $routeParams, $rootScope, $location) {
    return {
        get: function (reservation_id) {
            return $resource('/rest/reservations/:id', {
                id: reservation_id
            }).get(function(data) {
            }, function(error) {
                if(error.data.indexOf("ReservationExpiredException") > -1) {
                    bootbox.alert("Wybrana rezerwacja jest nieważna. Proszę spróbować ponownie", function() {
                        $rootScope.$apply( function(){$location.path('/'); } );
                    });
                }
                if(error.data.indexOf("NullPointerException") > -1) {
                    bootbox.alert("Wybrana rezerwacja nie została znaleziona. Proszę spróbować ponownie", function() {
                        $rootScope.$apply( function(){$location.path('/'); } );
                    });
                }
            });
        }
    }
});
$