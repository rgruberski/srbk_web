
srbkWebApp.controller('MainCtrl', function ($scope, moviesFactory) {

    $scope.movies = moviesFactory.query();

    $scope.getMovieById = function (id) {
        var movies = $scope.movies;
        for (var i = 0; i < movies.length; i++) {
            var movie = $scope.movies[i];
            if (movie.id == id) {
                $scope.currMovie = movie;
            }
        }
    };
})


srbkWebApp.controller('movieDetailsCtrl', function ($scope, $routeParams) {
    $scope.getMovieById($routeParams.id);
});

srbkWebApp.controller("bookTicketsController", function ($scope, $http, $location, $routeParams, moviesFactory, seancesFactory) {
    $scope.movies = moviesFactory.query();
    $scope.getMovieById($routeParams.id);
    $scope.product_id = $routeParams.id;
    $scope.seances = seancesFactory.query($routeParams.id);
});

srbkWebApp.controller("seatsChooseController", function ($scope, $http, $location, $routeParams, seatsFactory) {
    $scope.getMovieById($routeParams.id);
    $scope.product_id = $routeParams.id;
    $scope.seanse_id = $routeParams.sid;
    $scope.seance = seatsFactory.get();

    $scope.selectedSeats = [];

    $scope.changeSeatStatus = function(event, seat_id, remove) {

        if(!remove.no) {
            $scope.selectedSeats.push(seat_id);
        } else {
            var index = $scope.selectedSeats.indexOf(seat_id);

            if (index > -1) {
                $scope.selectedSeats.splice(index, 1);
            }
        }
    };

    $scope.doReservation = function(event) {

        $scope.reservation = {};

        $scope.reservation.seance = {"id" : $scope.seanse_id};

        $scope.reservation.tickets = [];

        $scope.selectedSeats.forEach(function(seat) {
            $scope.reservation.tickets.push({
                "seat" : {"id" : seat}
            });
        });

        $http({
            method: 'POST',
            url: '/rest/reservations/addPreliminaryReservation',
            data: JSON.stringify($scope.reservation)
        })
        .success(function (data) {
            $location.path('/bookTickets/' + $scope.product_id + '/' + $scope.seanse_id + '/' + data.id)
        })
        .error(function (data) {

            if(data.indexOf("ReservationBusySeatException") > -1) {
                bootbox.alert("Niestety wybrane miejsca zostały zarezerwowane przez kogoś innego. Proszę wybrać inne miejsca.", function() {
                    $scope.selectedSeats = [];
                    $scope.seance = seatsFactory.get();
                });
            }
        });
    }
});

srbkWebApp.controller("reservationController", function ($scope, $http, $location, $routeParams, seatsFactory, reservationsFactory) {

    $scope.getMovieById($routeParams.id);
    $scope.seance = seatsFactory.get();
    $scope.reservation = reservationsFactory.get($routeParams.prid);

    console.info($scope.reservation);

    $scope.product_id = $routeParams.id;
    $scope.seanse_id = $routeParams.sid;
    $scope.reservation_id = $routeParams.prid;

    $scope.confirm = function(event) {

        $scope.reservation.reservationStatus = "FILLED";

        $http({
            method: 'POST',
            url: '/rest/reservations/saveReservation',
            data: angular.toJson($scope.reservation)
        })
        .success(function (data) {
            $location.path('/bookTickets/' + $scope.product_id + '/' + $scope.seanse_id + '/' + data.id + '/confirm')
        })
        .error(function (data) {
            bootbox.alert("Wystąpił problem podczas składania rezerwacji, program przerywa prace. Proszę spróbować ponownie", function() {
                $location.path('/');
            });
        });
    }

    $scope.finalConfirm = function(event) {

        $scope.reservation.reservationStatus = "CONFIRMED";

        $http({
            method: 'POST',
            url: '/rest/reservations/saveReservation',
            data: angular.toJson($scope.reservation)
        })
        .success(function (data) {
            $location.path('/bookTickets/' + $scope.product_id + '/' + $scope.seanse_id + '/' + data.id + '/done')
        })
        .error(function (data) {
            bootbox.alert("Wystąpił problem podczas składania rezerwacji, program przerywa prace. Proszę spróbować ponownie", function() {
                $location.path('/');
            });
        });
    }
});